class AddDefaultActivityTypes < ActiveRecord::Migration
  def change
    ActivityType.create(name: 'English lesson')
    ActivityType.create(name: 'Skype call')
    ActivityType.create(name: 'Meeting')
  end
end
