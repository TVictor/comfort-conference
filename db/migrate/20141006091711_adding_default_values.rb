class AddingDefaultValues < ActiveRecord::Migration
  def up
    change_column(:rooms, :name, :string, null: false)

    change_column(:activity_types, :name, :string, null: false)

    change_column(:reservations, :start, :datetime, null: false)
    change_column(:reservations, :finish, :datetime, null: false)
    change_column(:reservations, :username, :string, null: false)
    change_column(:reservations, :active, :boolean, {null: false, default: true})
  end

  def down
    change_column(:rooms, :name, :string)

    change_column(:activity_types, :name, :string)

    change_column(:activity_types, :name, :string)

    change_column(:reservations, :start, :datetime)
    change_column(:reservations, :finish, :datetime)
    change_column(:reservations, :username, :string)
    change_column(:reservations, :active, :boolean)
  end
end
