class AddNameplateImageToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :nameplate_image, :string
  end
end
