class FixingTimeType < ActiveRecord::Migration
  def up
    change_table :reservations do |t|
      t.change :start, :datetime
      t.change :finish, :datetime
    end
  end

  def down
    change_table :reservations do |t|
      t.change :start, :time
      t.change :finish, :time
    end
  end
end
