class AddBigPictureNameToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :preview_name, :string
  end
end
