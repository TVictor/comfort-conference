class AddPeopleQuantityToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :people_quantity, :integer
  end
end
