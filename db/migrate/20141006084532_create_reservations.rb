class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.references :room, index: true
      t.time :start
      t.time :finish
      t.references :activity_type, index: true
      t.string :username
      t.string :secretword
      t.text :comment
      t.boolean :active

      t.timestamps
    end
  end
end
