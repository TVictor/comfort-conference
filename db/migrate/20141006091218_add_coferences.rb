class AddCoferences < ActiveRecord::Migration
  def change
    Room.create(name: "UC DAVIS", description: "Yellow conference", people_quantity: 5, image: "us_davis.jpeg", nameplate_image: "us_davis_np.jpeg")
    Room.create(name: "STANDFORD", description: "Blue conference", people_quantity: 30, image: "standford.jpeg", nameplate_image: "standford_np.jpeg")
    Room.create(name: "HARVARD", description: "Green conference", people_quantity: 5, image: "harvard.jpeg", nameplate_image: "harvard_np.jpeg")
    Room.create(name: "BERKLEY", description: "The little conference", people_quantity: 2, image: "berkley.jpeg", nameplate_image: "berkley_np.jpeg")
    Room.create(name: "YALE", description: "Triangle conference", people_quantity: 1, image: "yale.jpeg", nameplate_image: "yale_np.jpeg")
    Room.create(name: "Table tennis", description: "Just have a fun", people_quantity: 4, image: "tenis.jpeg", nameplate_image: "tenis_np.jpeg")
  end
end
