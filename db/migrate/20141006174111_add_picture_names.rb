class AddPictureNames < ActiveRecord::Migration
  def change
    Room.find_by_name('UC DAVIS').update_attribute(:preview_name, 'us_davis_preview.jpeg')
    Room.find_by_name('STANDFORD').update_attribute(:preview_name, 'standford_preview.jpeg')
    Room.find_by_name('HARVARD').update_attribute(:preview_name, 'harvard_preview.jpeg')
    Room.find_by_name('BERKLEY').update_attribute(:preview_name, 'berkley_preview.jpeg')
    Room.find_by_name('YALE').update_attribute(:preview_name, 'yale_preview.jpeg')
    Room.find_by_name('Table tennis').update_attribute(:preview_name, 'tenis_preview.jpeg')
  end
end
