# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141006122626) do

  create_table "activity_types", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservations", force: true do |t|
    t.integer  "room_id"
    t.datetime "start",                           null: false
    t.datetime "finish",                          null: false
    t.integer  "activity_type_id"
    t.string   "username",                        null: false
    t.string   "secretword"
    t.text     "comment"
    t.boolean  "active",           default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reservations", ["activity_type_id"], name: "index_reservations_on_activity_type_id"
  add_index "reservations", ["room_id"], name: "index_reservations_on_room_id"

  create_table "rooms", force: true do |t|
    t.string   "name",            null: false
    t.string   "description"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "people_quantity"
    t.string   "nameplate_image"
  end

end
