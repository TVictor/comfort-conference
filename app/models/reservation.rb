class Reservation < ActiveRecord::Base
  belongs_to :room
  belongs_to :activity_type

  validates_associated :room
  validates_associated :activity_type

  validates :room, presence: true
  validates :activity_type, presence: true
  validates :username, presence: true

  validate :time_period

  scope :today_reservations, -> { where(active: true, start: Time.now.beginning_of_day..(Time.now.end_of_day)) }

  protected
  def time_period
    errors.add(:start, 'can\'t be blank') if start.blank?
    errors.add(:finish, 'can\'t be blank') if finish.blank?
    return if start.nil? || finish.nil?

    if(start >= finish)
      errors.add(:start, 'should be earlier than finish')
      errors.add(:finish, 'should be later than start')
      return
    end

    today_for_this_room = Reservation.today_reservations.where(room: room).where.not(id: id)

    fail = false
    today_for_this_room.each do |existing_reservation|
      if(start >= existing_reservation.start && start < existing_reservation.finish)
        errors.add(:start, 'is in existing reservation')
        fail = true
      end

      if(finish > existing_reservation.start && finish <= existing_reservation.finish)
        errors.add(:finish, 'is in existing reservation')
        fail = true
      end
      return if fail

      if(existing_reservation.start >= start && existing_reservation.start < finish)
        errors.add(:start, 'Existing reservation is in time range')
        errors.add(:finish, 'Existing reservation is in time range')
        return
      end
    end
  end

end
