class Room < ActiveRecord::Base
  has_many :reservations
  validates :name, presence: true
  has_many :today_reservations, -> { where(active: true, start: Time.now.beginning_of_day..(Time.now.end_of_day)) }, class_name: 'Reservation'

  # statuses: free, busy, nearly_free, nearly_busy
  def reservation_status
    now_time = Time.now

    today_reserved = reservations.where(active: true).where("finish > ?", now_time).order(:start)
    return :free if today_reserved.blank?

    if today_reserved.first.start <= now_time
      if (today_reserved.first.finish - now_time) >= 15.minutes
        return :busy
      else
        if today_reserved.second.nil? || (today_reserved.second.start - now_time) > 30.minutes
          return :nearly_free
        else
          return :busy
        end
      end
    else
      if (today_reserved.first.start - now_time) >= 15.minutes
        return :free
      else
        return :nearly_busy
      end
    end
  end
end
