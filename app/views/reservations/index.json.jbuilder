json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :room_id, :start, :finish, :activity_type_id, :username, :secretword, :comment, :active
  json.url reservation_url(reservation, format: :json)
end
