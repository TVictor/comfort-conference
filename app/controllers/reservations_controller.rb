class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
    start = Time.now.midnight + params[:start].to_i.minutes
    @reservation = Reservation.new({room_id: params[:room_id], start: start, finish: start + 30.minutes, secretword: cookies[:secretword]})
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)

    respond_to do |format|
      if @reservation.save
        cookies[:secretword] = @reservation.secretword unless @reservation.secretword.blank?
        format.html { redirect_to room_path(@reservation[:room_id]), notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    if @reservation.secretword.present? && @reservation.secretword != cookies[:secretword]
      if @reservation.secretword != params[:secretword]
        flash[:danger] = 'Secret word is wrong.' if params[:secretword].present?
        render('_form_secretword') && return
      end
    end
    @reservation.update_attributes(active: false)
    respond_to do |format|
      format.html { redirect_to room_path(@reservation.room), notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:room_id, :start, :finish, :activity_type_id, :username, :secretword, :comment, :active)
    end
end
