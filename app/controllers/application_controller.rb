class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :all_room, :activity_type, only: [:new, :create, :update]


  private
    def all_room
      @room ||= Room.all
    end

    def activity_type
      @activity_type ||= ActivityType.all
    end
end
