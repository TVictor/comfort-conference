$(document).ready ->
  $('.reservations').on 'mousemove', (e)->
    $('.reservation.new').remove();
    offset = (e.offsetY || e.pageY - $(e.target).offset().top) // 40
    item = $("<a href=" +$('.reservations').data('new') + "&start=#{ offset * 30 + 540 } class='reservation new'>").html('reserve')
    item.css({top: "#{ offset * 40 }px" })
    $('.reservations').append(item)
    $('.reservation.new').on 'mousemove', (e)->
      e.stopPropagation()
    $('.reservation.new').on 'mouseleave', (e)->
      $('.reservation.new').remove();

  $('.reservation').on 'mousemove', (e)->
    e.stopPropagation()

#  $('.reservation').on 'click', (e)->
#    e.stopPropagation() if $(e.target)[0].nodeName != 'A'
